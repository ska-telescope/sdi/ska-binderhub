BASE_DIR=$(shell pwd)
OCI_IMAGE_BUILD_CONTEXT="./" # bypass bug on OCI images makefile

-include .make/base.mk
-include .make/release.mk
-include .make/oci.mk
-include .make/helm.mk
-include .make/k8s.mk

-include PrivateRules.mak

NAME=binderhub
